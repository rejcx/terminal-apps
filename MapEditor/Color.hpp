#ifndef _COLOR
#define _COLOR

#include <ncurses.h>

class Color
{
    public:
    void Setup( const std::string& name, int index, short fore, short bg )
    {
        this->index = index;
        this->name = name;
        init_pair( index, fore, bg );
    }

    int index;
    std::string name;
};

#endif
