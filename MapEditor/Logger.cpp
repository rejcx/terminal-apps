#include "Logger.hpp"

std::ofstream Logger::m_out;

void Logger::Init()
{
    m_out.open( "log.txt" );
    m_out << "Log Begin" << std::endl;
}

void Logger::Log( const std::string& text )
{
    m_out << text << std::endl;
}

void Logger::Close()
{
    m_out.close();
}
