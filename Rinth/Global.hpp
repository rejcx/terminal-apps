#ifndef _GLOBAL_HPP
#define _GLOBAL_HPP

#include <fstream>
#include <sstream>
using namespace std;

#include <cstdlib>
#include <ctime>

const int SCREEN_WIDTH = 80;
const int SCREEN_HEIGHT = 24;
ofstream LOG;

enum Direction { UNSET, NORTH, SOUTH, EAST, WEST };
enum TileType { OUTOFBOUNDS, CEILING, WALL, FLOOR, ROCK, ENEMY };
enum Element { COAL=0, IRON=1, SILVER=2, GOLD=3 };

void SeedRandomGenerator()
{
    srand( time( NULL ) );
}

int Random( int min, int max )
{
    // I think this is it lol
    int diff = max - min;
    return rand() % diff + min;
}

Element GetRandomElement()
{
    int type = Random( 0, 4 );
    return (Element)type;
}

string ElementToString( Element element )
{
    if      ( element == COAL )      { return "coal"; }
    else if ( element == IRON )      { return "iron"; }
    else if ( element == SILVER )    { return "silver"; }
    else if ( element == GOLD )      { return "gold"; }
}

int Min( int a, int b )
{
    if ( a < b )
        return a;
    else
        return b;
}

int Max( int a, int b )
{
    if ( a > b )
        return a;
    else
        return b;
}

string IntToStr( int value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}

#endif
