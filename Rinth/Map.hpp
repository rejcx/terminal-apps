#ifndef _MAP_HPP
#define _MAP_HPP

#include "Entity.hpp"
#include "Global.hpp"

#include <vector>
using namespace std;

struct Room
{
    int x, y;
    int expandX, expandY;
    Entity* ptrEntity;
};

class Map
{
public:
    Map() { }

    void Setup( int ceiling, int wall, int floor, int rocks )
    {
        m_ceilingColorCode = ceiling;
        m_wallColorCode = wall;
        m_floorColorCode = floor;
        m_rockColorCode = rocks;
    }

    int GetFloorColor()
    {
        return m_floorColorCode;
    }

    void GenerateMap( Terminal& terminal )
    {
        m_rooms.clear();

        // Create empty tiles first...
        for ( int y = 0; y < SCREEN_HEIGHT; y++ )
        {
            for ( int x = 0; x < SCREEN_WIDTH; x++ )
            {
                m_tiles[x][y].Setup( " ", m_ceilingColorCode );
                m_tiles[x][y].SetPosition( x, y );
            }
        }

        // Create rooms
        int roomCount = Random( 5, 10 );
        string roomSymbol = "";
        LOG << "Room count: " << roomCount << endl;

        for ( int i = 0; i < roomCount; i++ )
        {
            roomSymbol = IntToStr( i );

            Room newRoom;
            // Choose a location for this room
            newRoom.x = Random( 0, SCREEN_WIDTH );
            newRoom.y = Random( 1, SCREEN_HEIGHT ); // Make space for a wall on top

            // Expand the room size around the centerpoint
            newRoom.expandX = Random( 3, 10 );
            newRoom.expandY = Random( 4, 8 );

            for ( int ex = 0; ex < newRoom.expandX; ex++ )
            {
                for ( int ey = 0; ey < newRoom.expandY; ey++ )
                {
                    if ( newRoom.x + ex < SCREEN_WIDTH && newRoom.y + ey < SCREEN_HEIGHT )
                    {
                        m_tiles[newRoom.x + ex][newRoom.y + ey].Setup( roomSymbol, m_floorColorCode );
                    }
                }
            }

            m_rooms.push_back( newRoom );
        }

        // Create paths between each room, 0 -> 1, 1 -> 2, etc.
        int x;
        int y;
        for ( auto i = 0; i < m_rooms.size() - 1; i++ )
        {
            LOG << endl << "Attach room " << i << " to room " << i+1 << "..." << endl;
            LOG << "\t Room " << i     << ": " << m_rooms[i].x << ", " << m_rooms[i].y << endl;
            LOG << "\t Room " << i+1   << ": " << m_rooms[i+1].x << ", " << m_rooms[i+1].y << endl;

            roomSymbol = IntToStr( i );

            int startX = m_rooms[i].x;
            int startY = m_rooms[i].y;

            int endX = m_rooms[i+1].x;
            int endY = m_rooms[i+1].y;

            x = startX;
            y = startY;

            // Move X first, then Y.
            while ( x != endX )
            {
                m_tiles[x][y].Setup( roomSymbol, m_floorColorCode );

                if ( endX < x )
                {
                    x--;
                }
                else
                {
                    x++;
                }
            }

            while ( y != endY )
            {
                m_tiles[x][y].Setup( roomSymbol, m_floorColorCode );

                if ( endY < y )
                {
                    y--;
                }
                else
                {
                    y++;
                }
            }

            LOG << endl << endl;
        }

        // Add walls above floors
        for ( int y = 0; y < SCREEN_HEIGHT; y++ )
        {
            for ( int x = 0; x < SCREEN_WIDTH; x++ )
            {
                // Is this a floor?
                if ( m_tiles[x][y].GetColor() == m_floorColorCode )
                {
                    // Is there just "ceiling" directly above? (replace it with a wall.)
                    if ( y-1 >= 0 && m_tiles[x][y-1].GetColor() == m_ceilingColorCode )
                    {
                        m_tiles[x][y-1].SetColor( m_wallColorCode );
                    }
                }
            }
        }

        // Generate "rocks" on the floors
        int rockCount = Random( 5, 10 );
        for ( int i = 0; i < rockCount; i++ )
        {
            // hmm, find a floor to put it in? I guess we could look at rooms.
            int whichRoom = Random( 0, m_rooms.size() );

            int x = Random( m_rooms[whichRoom].x, m_rooms[whichRoom].x + m_rooms[whichRoom].expandX );
            int y = Random( m_rooms[whichRoom].y, m_rooms[whichRoom].y + m_rooms[whichRoom].expandY );

            m_tiles[x][y].Setup( "@", m_rockColorCode );
        }
    }

    Room& GetFirstRoom()
    {
        return m_rooms[0];
    }

    Room& GetLastRoom()
    {
        return m_rooms[ m_rooms.size() - 1];
    }

    TileType GetTileType( int tx, int ty, Direction dir )
    {
        int x = tx;
        int y = ty;

        if      ( dir == NORTH ) { y--; }
        else if ( dir == SOUTH ) { y++; }
        else if ( dir == WEST )  { x--; }
        else if ( dir == EAST )  { x++; }

        if ( x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT )
        {
            return OUTOFBOUNDS;
        }

        if      ( m_tiles[x][y].GetColor() == m_floorColorCode )    { return FLOOR; }
        else if ( m_tiles[x][y].GetColor() == m_wallColorCode )     { return WALL; }
        else if ( m_tiles[x][y].GetColor() == m_ceilingColorCode )  { return CEILING; }
        else if ( m_tiles[x][y].GetColor() == m_rockColorCode )     { return ROCK; }
    }

    void Draw( Terminal& terminal )
    {
        for ( int y = 0; y < SCREEN_HEIGHT; y++ )
        {
            for ( int x = 0; x < SCREEN_WIDTH; x++ )
            {
                m_tiles[x][y].Draw( terminal );
            }
        }
    }

    void Mine( int x, int y, Direction direction )
    {
        // Trying to mine in [Direction] from (x, y)
        if ( direction == NORTH )
        {
            m_tiles[x][y-1].Setup( " ", m_floorColorCode );
        }
        else if ( direction == SOUTH )
        {
            m_tiles[x][y+1].Setup( " ", m_floorColorCode );
        }
        else if ( direction == EAST )
        {
            m_tiles[x+1][y].Setup( " ", m_floorColorCode );
        }
        else if ( direction == WEST )
        {
            m_tiles[x-1][y].Setup( " ", m_floorColorCode );
        }
    }

private:
    Entity m_tiles[SCREEN_WIDTH][SCREEN_HEIGHT];
    vector<Room> m_rooms;

    int m_floorColorCode;
    int m_wallColorCode;
    int m_ceilingColorCode;
    int m_rockColorCode;
};

#endif
