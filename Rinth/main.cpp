#include "Terminal.hpp"
#include "Entity.hpp"
#include "Global.hpp"
#include "Map.hpp"

#include <string>
using namespace std;

int main()
{
    SeedRandomGenerator();
    LOG.open( "log.txt" );

    Terminal terminal;

    Entity player( "@", terminal.GetColor( "yellow", "yellow" ) );
    Entity stairsDown( "v", terminal.GetColor( "red", "white" ) );

    string lastEvent = "";

    Map map;
    map.Setup(
        terminal.GetColor( "black", "black" ), // ceiling
        terminal.GetColor( "cyan", "cyan" ), // wall
        terminal.GetColor( "blue", "blue" ),  // floor
        terminal.GetColor( "white", "white" ) // rocks
    );
    map.GenerateMap( terminal );

    Room startRoom = map.GetFirstRoom();
    Room endRoom = map.GetLastRoom();

    player.SetPosition( startRoom.x, startRoom.y );
    stairsDown.SetPosition( endRoom.x, endRoom.y );

    while ( !terminal.IsDone() )
    {
        // Drawing
        terminal.StartDraw();

        map.Draw( terminal );
        stairsDown.Draw( terminal );

        player.Draw( terminal );

        if ( lastEvent != "" )
        {
            terminal.Draw( 0, 0, lastEvent, terminal.GetColor( "black", "white" ) );
        }

        player.DrawInventory( terminal );
        terminal.EndDraw();

        // Input
        int input = terminal.GetKey();
        Direction moveDirection = UNSET;

        if      ( input == KEY_LEFT )   { moveDirection = WEST; }
        else if ( input == KEY_RIGHT )  { moveDirection = EAST; }
        else if ( input == KEY_UP )     { moveDirection = NORTH; }
        else if ( input == KEY_DOWN )   { moveDirection = SOUTH; }

        TileType neighbor = map.GetTileType( player.GetX(), player.GetY(), moveDirection );

        if ( moveDirection != UNSET && ( neighbor == FLOOR ) )
        {
            player.Move( moveDirection );
        }
        else if ( moveDirection != UNSET && ( neighbor == ROCK ) )
        {
            Element randomElement = GetRandomElement();
            player.Mine( moveDirection, randomElement );
            map.Mine( player.GetX(), player.GetY(), moveDirection );
            lastEvent = "Found " + ElementToString( randomElement ) + "!";
        }

        if ( input == KEY_F(2) )
        {
            map.GenerateMap( terminal );

            startRoom = map.GetFirstRoom();
            endRoom = map.GetLastRoom();
            player.SetPosition( startRoom.x, startRoom.y );
            stairsDown.SetPosition( endRoom.x, endRoom.y );
        }
        else if ( input == KEY_F(4) )
        {
            terminal.Quit();
        }
    }

    LOG.close();

    return 0;
}
