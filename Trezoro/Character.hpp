#ifndef _CHARACTER
#define _CHARACTER

enum CharacterType { SPRITE_HUMAN, SPRITE_TREASURE };

class Character
{
    public:
    Character();
    void Setup( int x, int y, char symbol, int color );
    void Setup( char symbol, int color );
    void Setup( CharacterType type, int color );
    void SetPosition( int x, int y );
    void SetRandomPosition( int minX, int minY, int maxX, int maxY );
    void AddToScore();
    void Draw();
    void HandleKeypress( int k, int minX, int minY, int maxX, int maxY );

    int GetX();
    int GetY();
    int GetScore();

    private:
    void ClearSprite();
    int m_x, m_y;
    char m_symbol[3][3];
    int m_color;
    int m_step;
    int m_score;
};

#endif
