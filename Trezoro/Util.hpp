#ifndef _UTIL
#define _UTIL

#include <string>
#include <sstream>

class Util
{
    public:
    static std::string IntToString( int val )
    {
        std::stringstream ss;
        ss << val;
        return ss.str();
    }
};

#endif
