#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string>

#include "Map.hpp"
#include "Character.hpp"
#include "Util.hpp"

std::string GetRank( int score );

int main()
{
	initscr();
	keypad( stdscr, TRUE );
	curs_set( 0 );
	srand( time( NULL ) );

	// stdscr - 80x25
	int screenWidth = 80;
	int screenHeight = 20;

	if ( has_colors() )
	{
        start_color();
	}

    init_pair( FENCE,   COLOR_RED,      COLOR_GREEN );
    init_pair( WATER,   COLOR_BLUE,     COLOR_GREEN );
    init_pair( FLOWER,  COLOR_MAGENTA,  COLOR_GREEN );
    init_pair( GRASS,   COLOR_WHITE,    COLOR_GREEN );
    init_pair( EMPTY,   COLOR_BLACK,    COLOR_BLACK );
    init_pair( HUMAN,   COLOR_RED,      COLOR_GREEN );
    init_pair( TREASURE,COLOR_WHITE,   COLOR_YELLOW );
    init_pair( HUD,     COLOR_WHITE,    COLOR_BLACK );

    Map gameBoard;
    gameBoard.Generate( screenWidth, screenHeight );

    Character player;
    player.Setup( SPRITE_HUMAN, HUMAN );
    player.SetPosition( screenWidth / 2, screenHeight / 2 );

    Character treasure;
    treasure.Setup( SPRITE_TREASURE, TREASURE );
    treasure.SetRandomPosition( 1, 1, screenWidth-3, screenHeight-4 );

	bool done = false;
    int keypress;
    std::string scoreText;
	while ( !done )
	{
        clear();

        gameBoard.Draw();
        treasure.Draw();
        player.Draw();

        scoreText = " S C O R E: " + Util::IntToString( player.GetScore() ) + " ";
        attron( COLOR_PAIR( HUD ) );
        attron( A_BOLD );
        mvprintw( 21, 0, scoreText.c_str() );
        attroff( A_BOLD );
        attroff( COLOR_PAIR( HUD ) );
        scoreText = " R A N K: \"" + GetRank( player.GetScore() ) + "\"";
        attron( COLOR_PAIR( HUD ) );
        attron( A_BOLD );
        mvprintw( 21, screenWidth/2, scoreText.c_str() );
        attroff( A_BOLD );
        attroff( COLOR_PAIR( HUD ) );

        keypress = getch();

        if ( keypress == KEY_F( 1 ) )
        {
            done = true;
        }
        player.HandleKeypress( keypress, 1, 1, screenWidth-3, screenHeight-3 );

        // Collision with treasure?
        if ( player.GetX() == treasure.GetX() && player.GetY() == treasure.GetY() )
        {
            player.AddToScore();
            treasure.SetRandomPosition( 2, 2, screenWidth-3, screenHeight-4 );
        }
	}

    refresh();
	getch();
	endwin();
	return 0;
}

std::string GetRank( int score )
{
    if ( score < 5 )
    {
        return "l a m e r";
    }
    else if ( score < 10 )
    {
        return "b e g i n n e r";
    }
    else if ( score < 20 )
    {
        return "t r a m p l e r";
    }
    else if ( score < 40 )
    {
        return "h u n t e r";
    }
}
